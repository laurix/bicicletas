var map = L.map('main_map').setView([-38.717639, -62.265620],13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href: "https://www.openstreetmap.org/copyright"> OpenStreetMap </a> contributors'
}).addTo(map);

var uns = L.marker([-38.701967, -62.270369], {title: 'Universidad Nacional del Sur' } ).addTo(map);

var esc = L.marker([-38.710567, -62.265088], {title: 'Escuela Superior de Comercio' }).addTo(map);

var mbb = L.marker([-38.718276, -62.264268], {title: 'Municipalidad de Bahía Blanca' }).addTo(map);



$.ajax({
    dataType: "json",
    url: "api/bicicletas",  
    success: function(result){
        //console.log(result);
        result.bicicletas.forEach(function(bici){
            console.log("bici: " + bici.id);
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
            
        });
    }

})
